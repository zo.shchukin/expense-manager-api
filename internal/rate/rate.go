package rate

type ExchangeRates struct {
	Success   bool               `json:"success"`
	Timestamp int64              `json:"timestamp"`
	Source    string             `json:"source"`
	Quotes    map[string]float64 `json:"quotes"`
}

type RateProvider interface {
	GetExchangeRates() (*ExchangeRates, error)
}

type RateService interface {
	GetExchangeRates() (*ExchangeRates, error)
}

type rateService struct {
	repo RateProvider
}

func NewRateService(repo RateProvider) RateService {
	return &rateService{
		repo: repo,
	}
}

func (s *rateService) GetExchangeRates() (*ExchangeRates, error) {

	return s.repo.GetExchangeRates()
}
