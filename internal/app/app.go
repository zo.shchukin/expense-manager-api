package app

import (
	"crypto/rand"
	"crypto/rsa"
	"database/sql"
	"fmt"
	"os"

	"github.com/gorilla/mux"
	"gitlab.com/project-una/bb-api/internal/account"
	"gitlab.com/project-una/bb-api/internal/delivery/router"
	"gitlab.com/project-una/bb-api/internal/rate"
	"gitlab.com/project-una/bb-api/internal/storage/apilayer"
	"gitlab.com/project-una/bb-api/internal/storage/mysql"
	"gitlab.com/project-una/bb-api/internal/token"
	"gitlab.com/project-una/bb-api/internal/user"
)

func Run(db *sql.DB) (*mux.Router, error) {
	config := GetConfig()

	privateKey, err := rsa.GenerateKey(rand.Reader, 2048)
	if err != nil {
		return nil, err
	}

	validatorRepo := mysql.NewValidatorRepository(db)
	validatorService := account.NewValidatorService(validatorRepo)

	rateApiKey := os.Getenv("RATE_ACCESS_KEY")
	urlOfRate := fmt.Sprintf(
		config.Rate.Baseurl, config.Rate.Endpoint,
		rateApiKey, config.Rate.Currencies,
		config.Rate.Source, config.Rate.Format,
	)

	rateStorage := apilayer.NewRateRateProvider(urlOfRate)
	rateService := rate.NewRateService(rateStorage)

	transactionRepo := mysql.NewTransactionRepository(db)
	transactionService := account.NewTransactionService(transactionRepo, validatorService)

	accountRepo := mysql.NewAccountRepository(db)
	accountService := account.NewAccountService(accountRepo, validatorService)

	tokenRepo := mysql.NewTokenRepo(db)
	tokenService := token.NewTokenService(tokenRepo, privateKey)

	userRepository := mysql.NewUserRepository(db)
	userService := user.NewService(userRepository, tokenService)

	router := router.NewRouter(rateService, transactionService, accountService, userService, tokenService)

	return router, nil
}
