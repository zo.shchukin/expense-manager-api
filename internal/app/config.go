package app

import (
	"log"
	"os"

	"gopkg.in/yaml.v2"
)

type Config struct {
	Http struct {
		Port int
	}
	DB struct {
		Server        string
		Username      string
		DbName        string
		ConnectionStr string
	}
	Rate struct {
		Baseurl    string
		Endpoint   string
		Accesskey  string
		Currencies string
		Source     string
		Format     int
	}
	MigratePath string
}

func GetConfig() *Config {
	configPath := os.Getenv("CONFIG_PATH")

	if configPath == "" {
		log.Fatal("CONFIG_PATH not found.")
	}

	yamlFile, err := os.ReadFile(configPath)
	if err != nil {
		log.Fatalf("yamlFile.Get %+v ", err)
	}
	c := &Config{}
	err = yaml.Unmarshal(yamlFile, c)
	if err != nil {
		log.Fatalf("Unmarshal: %#v", err)
	}

	return c
}
