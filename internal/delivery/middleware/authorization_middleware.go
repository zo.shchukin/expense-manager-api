package middleware

import (
	"context"
	"net/http"
	"strconv"
	"strings"

	"gitlab.com/project-una/bb-api/internal/user"
)

func AuthMiddleware(tokenSvc user.TokenService) func(http.Handler) http.Handler {
	return func(next http.Handler) http.Handler {
		return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {

			excludedPaths := []string{"/authorization", "/refresh", "/user", "/"}

			for _, path := range excludedPaths {
				if r.URL.Path == path {
					next.ServeHTTP(w, r)
					return
				}
			}

			token := GetTokenFromHeader(r, "Authorization")
			if token == "" {
				http.Error(w, "you need authorization", http.StatusUnauthorized)
				return
			}

			userID, err := tokenSvc.Parse(token)
			if err != nil {
				http.Error(w, err.Error(), http.StatusUnauthorized)
				return
			}

			id, err := strconv.Atoi(userID)
			if err != nil {
				http.Error(w, err.Error(), http.StatusUnauthorized)
				return
			}

			ctx := context.WithValue(r.Context(), user.UserIDKey, id)

			next.ServeHTTP(w, r.WithContext(ctx))
		})
	}
}

func GetTokenFromHeader(r *http.Request, tp string) string {
	authHeader := r.Header.Get(tp)
	if authHeader == "" {
		return ""
	}
	if !strings.HasPrefix(authHeader, "Bearer ") {
		return ""
	}
	return strings.TrimPrefix(authHeader, "Bearer ")
}
