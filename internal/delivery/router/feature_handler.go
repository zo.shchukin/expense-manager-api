package router

import (
	"encoding/json"
	"net/http"

	"gitlab.com/project-una/bb-api/internal/rate"
)

type RateHandler struct {
	service rate.RateService
}

func NewRateHandler(service rate.RateService) *RateHandler {
	return &RateHandler{service}
}

func (fh *RateHandler) GetExchangeRates(w http.ResponseWriter, r *http.Request) {
	exchangeRates, err := fh.service.GetExchangeRates()
	if err != nil {
		http.Error(w, err.Error(), http.StatusInternalServerError)
		return
	}

	w.Header().Set("Content-Type", "application/json")
	json.NewEncoder(w).Encode(exchangeRates)
}
