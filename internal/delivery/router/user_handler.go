package router

import (
	"encoding/json"
	"net/http"

	"gitlab.com/project-una/bb-api/internal/user"
)

type UserHandler struct {
	service user.UserService
}

func NewUserHandler(service user.UserService) *UserHandler {
	return &UserHandler{service}
}

func (uh *UserHandler) AuthorizationUser(w http.ResponseWriter, r *http.Request) {
	var authForm user.AuthForm

	err := json.NewDecoder(r.Body).Decode(&authForm)
	if err != nil {
		http.Error(w, err.Error(), http.StatusBadRequest)
		return
	}

	userResponse, err := uh.service.AuthorizationUser(&authForm)
	if err != nil {
		http.Error(w, err.Error(), http.StatusBadRequest)
		return
	}

	w.Header().Set("Content-Type", "application/json")
	json.NewEncoder(w).Encode(userResponse)
}

func (uh *UserHandler) CreateUser(w http.ResponseWriter, r *http.Request) {
	var user user.User

	err := json.NewDecoder(r.Body).Decode(&user)
	if err != nil {
		http.Error(w, err.Error(), http.StatusBadRequest)
		return
	}

	err = uh.service.CreateUser(&user)
	if err != nil {
		http.Error(w, err.Error(), http.StatusInternalServerError)
		return
	}
	w.Header().Set("Content-Type", "application/json")
	w.WriteHeader(http.StatusCreated)
}

func (uh *UserHandler) RefreshToken(w http.ResponseWriter, r *http.Request) {
	var token user.Tokens

	err := json.NewDecoder(r.Body).Decode(&token)
	if err != nil {
		http.Error(w, err.Error(), http.StatusBadRequest)
		return
	}

	tokens, err := uh.service.RefreshTokens(token.RefreshToken)
	if err != nil {
		http.Error(w, "Failed to generate token", http.StatusUnauthorized)
		return
	}
	if tokens.AccessToken == "" {
		http.Error(w, "Failed to generate token", http.StatusUnauthorized)
		return
	}

	w.Header().Set("Content-Type", "application/json")
	json.NewEncoder(w).Encode(tokens)
}
