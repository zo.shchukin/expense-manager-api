package router

import (
	"net/http"
	"strconv"

	"gitlab.com/project-una/bb-api/internal/account"
	"gitlab.com/project-una/bb-api/internal/delivery/middleware"
	"gitlab.com/project-una/bb-api/internal/rate"
	"gitlab.com/project-una/bb-api/internal/user"

	"github.com/gorilla/handlers"
	"github.com/gorilla/mux"
)

func NewRouter(featureService rate.RateService, transactionService account.TransactionService, accountService account.AccountService,
	userService user.UserService, tokenService user.TokenService) *mux.Router {

	router := mux.NewRouter()

	trHandler := NewTransactionHandler(transactionService)
	actHandler := NewAccountHandler(accountService)
	usHandler := NewUserHandler(userService)
	ftHandler := NewRateHandler(featureService)

	corsHandler := handlers.CORS(
		handlers.AllowedOrigins([]string{"http://localhost:3000", "https://bb.una-team.pro"}),
		handlers.AllowedMethods([]string{"GET", "POST", "PUT",
			"DELETE", "OPTIONS"}),
		handlers.AllowedHeaders([]string{"Origin", "Content-Type", "Accept", "Authorization"}),
	)

	router.Use(corsHandler)
	router.Use(middleware.AuthMiddleware(tokenService))
	router.Use(middleware.SetCORSHeadersMiddleware)

	router.HandleFunc("/", info)
	router.HandleFunc("/transactions/account/{id}", trHandler.GetTransactionsByAccountID).Methods("GET", "OPTIONS")
	router.HandleFunc("/transactions", trHandler.CreateTransaction).Methods("POST", "OPTIONS")
	router.HandleFunc("/transactions/{id}", trHandler.UpdateTransaction).Methods("PUT", "OPTIONS")
	router.HandleFunc("/transactions/{id}", trHandler.DeleteTransaction).Methods("DELETE", "OPTIONS")

	router.HandleFunc("/transactions/accounts/{id}/statement", trHandler.GetStatementOfAccount).Methods("GET", "OPTIONS")
	router.HandleFunc("/transactions/accounts/{id}/statistics", trHandler.GetCostAnalysis).Methods("GET", "OPTIONS")

	router.HandleFunc("/exchange", ftHandler.GetExchangeRates).Methods("GET", "OPTIONS")

	router.HandleFunc("/accounts", actHandler.GetAccountsOfUser).Methods("GET", "OPTIONS")
	router.HandleFunc("/accounts/{id}", actHandler.GetAccountByID).Methods("GET", "OPTIONS")
	router.HandleFunc("/accounts", actHandler.CreateAccount).Methods("POST", "OPTIONS")
	router.HandleFunc("/accounts/{id}", actHandler.UpdateAccount).Methods("PUT", "OPTIONS")
	router.HandleFunc("/accounts/{id}", actHandler.DeleteAccount).Methods("DELETE", "OPTIONS")

	router.HandleFunc("/user", usHandler.CreateUser).Methods("POST", "OPTIONS")
	router.HandleFunc("/authorization", usHandler.AuthorizationUser).Methods("POST", "OPTIONS")
	router.HandleFunc("/refresh", usHandler.RefreshToken).Methods("POST", "OPTIONS")

	http.Handle("/", router)
	return router
}

func info(w http.ResponseWriter, r *http.Request) {
	w.Write([]byte("This page provides information about the API"))
}

func getIdFromPath(w http.ResponseWriter, r *http.Request) (int64, error) {
	params := mux.Vars(r)
	idStr := params["id"]
	id, err := strconv.ParseInt(idStr, 10, 64)
	if err != nil || id == 0 {
		http.Error(w, "invalid ID", http.StatusBadRequest)
		return 0, err
	}
	return id, nil
}

func getUserIDFromToken(r *http.Request) int64 {
	return int64(r.Context().Value(user.UserIDKey).(int))
}
