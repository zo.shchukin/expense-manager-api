package router

import (
	"encoding/json"
	"net/http"

	"gitlab.com/project-una/bb-api/internal/account"
)

type AccountkHandler struct {
	service account.AccountService
}

func NewAccountHandler(service account.AccountService) *AccountkHandler {
	return &AccountkHandler{service}
}

func (ah *AccountkHandler) GetAccountsOfUser(w http.ResponseWriter, r *http.Request) {

	userID := getUserIDFromToken(r)

	accounts, err := ah.service.GetAllAccounts(userID)
	if err != nil {
		http.Error(w, err.Error(), http.StatusBadRequest)
		return
	}

	w.Header().Set("Content-Type", "application/json")
	json.NewEncoder(w).Encode(accounts)
}

func (ah *AccountkHandler) GetAccountByID(w http.ResponseWriter, r *http.Request) {

	userID := getUserIDFromToken(r)
	id, err := getIdFromPath(w, r)
	if err != nil {
		return
	}

	account, err := ah.service.GetAccountByID(userID, id)
	if err != nil {
		if err.Error() == "account not found" {
			http.NotFound(w, r)
		} else {
			http.Error(w, err.Error(), http.StatusBadRequest)
		}
		return
	}

	w.Header().Set("Content-Type", "application/json")
	json.NewEncoder(w).Encode(account)
}

func (ah *AccountkHandler) CreateAccount(w http.ResponseWriter, r *http.Request) {

	var account account.Account

	err := json.NewDecoder(r.Body).Decode(&account)
	if err != nil {
		http.Error(w, err.Error(), http.StatusBadRequest)
		return
	}
	account.UserID = getUserIDFromToken(r)

	createdAccount, err := ah.service.CreateAccount(&account)
	if err != nil {
		http.Error(w, err.Error(), http.StatusBadRequest)
		return
	}

	w.Header().Set("Content-Type", "application/json")
	w.WriteHeader(http.StatusCreated)
	json.NewEncoder(w).Encode(createdAccount)
}

func (ah *AccountkHandler) UpdateAccount(w http.ResponseWriter, r *http.Request) {
	var account account.Account
	err := json.NewDecoder(r.Body).Decode(&account)
	if err != nil {
		http.Error(w, err.Error(), http.StatusBadRequest)
		return
	}

	account.ID, err = getIdFromPath(w, r)
	if err != nil {
		return
	}
	userId := getUserIDFromToken(r)
	account.UserID = userId
	err = ah.service.UpdateAccount(userId, &account)
	if err != nil {
		http.Error(w, err.Error(), http.StatusBadRequest)
		return
	}

	w.WriteHeader(http.StatusNoContent)
}

func (ah *AccountkHandler) DeleteAccount(w http.ResponseWriter, r *http.Request) {

	var err error

	acId, err := getIdFromPath(w, r)
	if err != nil {
		return
	}
	userId := getUserIDFromToken(r)

	err = ah.service.DeleteAccount(userId, acId)
	if err != nil {
		http.Error(w, err.Error(), http.StatusBadRequest)
		return
	}

	w.WriteHeader(http.StatusNoContent)
}
