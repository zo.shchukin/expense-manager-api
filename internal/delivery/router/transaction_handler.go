package router

import (
	"encoding/json"
	"net/http"

	"gitlab.com/project-una/bb-api/internal/account"
)

type TransactionHandler struct {
	service account.TransactionService
}

func NewTransactionHandler(service account.TransactionService) *TransactionHandler {
	return &TransactionHandler{service}
}

func (h *TransactionHandler) GetStatementOfAccount(w http.ResponseWriter, r *http.Request) {

	userID := getUserIDFromToken(r)

	id, err := getIdFromPath(w, r)
	if err != nil {
		return
	}
	statements, err := h.service.GetStatementOfAccount(userID, id)
	if err != nil {
		http.Error(w, err.Error(), http.StatusBadRequest)
		return
	}

	w.Header().Set("Content-Type", "application/json")
	json.NewEncoder(w).Encode(statements)
}

func (h *TransactionHandler) GetCostAnalysis(w http.ResponseWriter, r *http.Request) {

	userID := getUserIDFromToken(r)
	acId, err := getIdFromPath(w, r)
	if err != nil {
		return
	}

	account, err := h.service.GetCostAnalysis(userID, acId)
	if err != nil {
		if err.Error() == "account not found" {
			http.NotFound(w, r)
		} else {
			http.Error(w, err.Error(), http.StatusBadRequest)
		}
		return
	}

	w.Header().Set("Content-Type", "application/json")
	json.NewEncoder(w).Encode(account)
}

func (h *TransactionHandler) GetTransactionsByAccountID(w http.ResponseWriter, r *http.Request) {
	userID := getUserIDFromToken(r)

	id, err := getIdFromPath(w, r)
	if err != nil {
		return
	}
	transactions, err := h.service.GetTransactionsByAccountID(userID, id)
	if err != nil {
		http.Error(w, err.Error(), http.StatusBadRequest)
		return
	}

	w.Header().Set("Content-Type", "application/json")
	json.NewEncoder(w).Encode(transactions)
}

func (h *TransactionHandler) CreateTransaction(w http.ResponseWriter, r *http.Request) {
	var transaction account.Transaction
	userID := getUserIDFromToken(r)
	err := json.NewDecoder(r.Body).Decode(&transaction)
	if err != nil {
		http.Error(w, err.Error(), http.StatusBadRequest)
		return
	}

	createdTransaction, err := h.service.CreateTransaction(userID, &transaction)
	if err != nil {
		http.Error(w, err.Error(), http.StatusBadRequest)
		return
	}

	w.Header().Set("Content-Type", "application/json")
	w.WriteHeader(http.StatusCreated)
	json.NewEncoder(w).Encode(createdTransaction)
}

func (h *TransactionHandler) UpdateTransaction(w http.ResponseWriter, r *http.Request) {
	userID := getUserIDFromToken(r)

	trId, err := getIdFromPath(w, r)
	if err != nil {
		return
	}

	var transaction account.Transaction
	transaction.ID = trId
	err = json.NewDecoder(r.Body).Decode(&transaction)
	if err != nil {
		http.Error(w, err.Error(), http.StatusBadRequest)
		return
	}

	err = h.service.UpdateTransaction(userID, &transaction)
	if err != nil {
		http.Error(w, err.Error(), http.StatusNotFound)
		return
	}

	w.WriteHeader(http.StatusNoContent)
}

func (h *TransactionHandler) DeleteTransaction(w http.ResponseWriter, r *http.Request) {
	userID := getUserIDFromToken(r)

	id, err := getIdFromPath(w, r)
	if err != nil {
		http.Error(w, err.Error(), http.StatusBadRequest)
		return
	}

	err = h.service.DeleteTransaction(userID, id)
	if err != nil {
		http.Error(w, err.Error(), http.StatusNotFound)
		return
	}

	w.WriteHeader(http.StatusNoContent)
}
