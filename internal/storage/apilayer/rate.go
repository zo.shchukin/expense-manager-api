package apilayer

// this API provides access to exchange rates
import (
	"encoding/json"
	"fmt"
	"net/http"
	"time"

	"gitlab.com/project-una/bb-api/internal/rate"
)

type ratePrvd struct {
	url  string
	rate map[time.Time]*rate.ExchangeRates
}

func NewRateRateProvider(url string) rate.RateProvider {
	return &ratePrvd{
		url:  url,
		rate: make(map[time.Time]*rate.ExchangeRates),
	}
}

func (r *ratePrvd) GetExchangeRates() (*rate.ExchangeRates, error) {
	var exchangeRates *rate.ExchangeRates
	today := time.Now().Truncate(24 * time.Hour)

	exchangeRates, ok := r.rate[today]
	if ok {
		return exchangeRates, nil
	}

	url := fmt.Sprintf(r.url)
	resp, err := http.Get(url)
	if err != nil {
		return nil, err
	}
	defer resp.Body.Close()

	if err := json.NewDecoder(resp.Body).Decode(&exchangeRates); err != nil {
		return nil, err
	}

	exchangeRates.Quotes["USDUSD"] = 1

	r.rate[today] = exchangeRates

	return exchangeRates, nil
}
