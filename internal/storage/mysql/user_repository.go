package mysql

import (
	"database/sql"

	"gitlab.com/project-una/bb-api/internal/user"
)

type userRepo struct {
	db *sql.DB
}

func NewUserRepository(db *sql.DB) user.UserRepository {
	return &userRepo{db}
}

func (ur *userRepo) GetUserByEmail(email string) (*user.User, error) {
	userDB := &UserDB{}
	row := ur.db.QueryRow("SELECT id, name, email, hash_psw FROM users WHERE email = ?", email)

	err := row.Scan(&userDB.ID, &userDB.Name, &userDB.Email, &userDB.HashPassword)
	if err != nil {
		return nil, err
	}

	return userDB.User(), nil
}

func (r *userRepo) CreateUser(user *user.User) error {
	userDB := GetUserDB(user)
	result, err := r.db.Exec("INSERT INTO users (name, email, hash_psw) VALUES (?, ?, ?)",
		userDB.Name, userDB.Email, userDB.HashPassword)
	if err != nil {
		return err
	}

	_, err = result.LastInsertId()
	if err != nil {
		return err
	}

	return nil
}
