package mysql

import (
	"context"
	"database/sql"
	"fmt"
	"time"

	"gitlab.com/project-una/bb-api/internal/account"
)

type transactionRepo struct {
	db *sql.DB
}

func NewTransactionRepository(db *sql.DB) account.TransactionRepository {
	return &transactionRepo{db}
}

func (tr *transactionRepo) GetStatementOfAccount(userId, accId int64) ([]*account.Statement, error) {
	ctx := context.Background()

	defer ctx.Done()
	conn, err := tr.db.Conn(ctx)
	if err != nil {
		return nil, err
	}
	defer conn.Close()

	_, err = conn.ExecContext(ctx, "SELECT @balance := SUM(amount) FROM transactions WHERE  account_id = ?;", accId)
	if err != nil {
		return nil, err
	}
	qwery := `SELECT id, account_id, description, tag, amount, date, 
			  @balance as balance, (@balance := @balance - amount) j FROM transactions		  
			  WHERE account_id = ? ORDER BY date DESC, id DESC, amount DESC;`
	rows, err := conn.QueryContext(ctx, qwery, accId)
	if err != nil {
		return nil, err
	}
	defer rows.Close()

	var (
		statements []*account.Statement
		i          float64
	)

	for rows.Next() {
		statementDB := &StatementDB{}
		err := rows.Scan(&statementDB.ID, &statementDB.AccountId, &statementDB.Description,
			&statementDB.Tag, &statementDB.Amount, &statementDB.Date, &statementDB.Balance, &i)
		if err != nil {
			return nil, err
		}
		statements = append(statements, statementDB.Statement())
	}

	return statements, nil
}

func (tr *transactionRepo) GetCostAnalysis(userId, accId int64) (map[string]float64, error) {
	ctx := context.Background()

	defer ctx.Done()
	conn, err := tr.db.Conn(ctx)
	if err != nil {
		return nil, err
	}
	defer conn.Close()
	currentTime := time.Now().Format("2006-01-02")
	oneMonthAgo := time.Now().AddDate(0, -1, 0).Format("2006-01-02")

	_, err = conn.ExecContext(ctx, `SELECT @expenses := SUM(amount) FROM transactions WHERE  account_id = ?
									AND date BETWEEN ? AND ?
									AND amount < 0;`, accId, oneMonthAgo, currentTime)
	if err != nil {
		return nil, err
	}
	qwery := `SELECT round(SUM(amount)/ @expenses, 2) * 100 , tag
			  FROM transactions
			  WHERE account_id = ? 
			  AND date BETWEEN ? AND ?
			  AND amount < 0
			  GROUP BY tag;`
	rows, err := conn.QueryContext(ctx, qwery, accId, oneMonthAgo, currentTime)
	if err != nil {
		return nil, err
	}
	defer rows.Close()

	costs := make(map[string]float64)
	for rows.Next() {
		var percent float64
		var description string
		err := rows.Scan(&percent, &description)
		if err != nil {
			panic(err)
		}
		costs[description] = percent
	}

	return costs, nil
}

func (tr *transactionRepo) GetAllTransactions(userId int64) ([]*account.Transaction, error) {

	rows, err := tr.db.Query(`SELECT * FROM transactions 
							 WHERE account_id IN (SELECT id FROM accounts WHERE user_id = ?)`, userId)
	if err != nil {
		return nil, err
	}
	defer rows.Close()

	var transactions []*account.Transaction
	for rows.Next() {
		transactionDB := &TransactionDB{}

		err := rows.Scan(&transactionDB.ID, &transactionDB.AccountId, &transactionDB.Description,
			&transactionDB.Tag, &transactionDB.Amount, &transactionDB.Date)
		if err != nil {
			return nil, err
		}
		transactions = append(transactions, transactionDB.Transaction())
	}

	return transactions, nil
}

func (tr *transactionRepo) GetTransactionsByAccountID(userId, acId int64) ([]*account.Transaction, error) {

	rows, err := tr.db.Query("SELECT * FROM transactions WHERE account_id = ?", acId)
	if err != nil {
		return nil, err
	}
	defer rows.Close()

	var transactions []*account.Transaction
	for rows.Next() {
		transactionDB := &TransactionDB{}

		err := rows.Scan(&transactionDB.ID, &transactionDB.AccountId, &transactionDB.Description,
			&transactionDB.Tag, &transactionDB.Amount, &transactionDB.Date)
		if err != nil {
			return nil, err
		}
		transactions = append(transactions, transactionDB.Transaction())
	}

	return transactions, nil
}

func (tr *transactionRepo) GetTransactionByID(userId, id int64) (*account.Transaction, error) {

	row := tr.db.QueryRow("SELECT * FROM transactions WHERE id = ?", id)

	transactionDB := &TransactionDB{}

	err := row.Scan(
		&transactionDB.ID, &transactionDB.AccountId, &transactionDB.Description,
		&transactionDB.Tag, &transactionDB.Amount, &transactionDB.Date,
	)
	if err != nil {
		if err == sql.ErrNoRows {
			return nil, fmt.Errorf("transaction not found")
		}
		return nil, err
	}

	return transactionDB.Transaction(), nil
}

func (tr *transactionRepo) CreateTransaction(transaction *account.Transaction) (*account.Transaction, error) {

	transactionDB := GetTransactionDB(transaction)

	result, err := tr.db.Exec(`INSERT INTO transactions (account_id, description, tag, amount, date)
							   VALUES (?, ?, ?, ?, ?)`,
		transaction.AccountId, transactionDB.Description, transactionDB.Tag,
		transactionDB.Amount, transactionDB.Date)
	if err != nil {
		return nil, err
	}

	transaction.ID, err = result.LastInsertId()
	if err != nil {
		return nil, err
	}

	return transaction, nil
}

func (tr *transactionRepo) UpdateTransaction(userId int64, transaction *account.Transaction) error {

	transactionDB := GetTransactionDB(transaction)

	res, err := tr.db.Exec(`UPDATE transactions SET account_id = ?, description = ?, tag = ?, amount = ?, date = ? 
						 	WHERE id = ?`,
		transactionDB.AccountId, transactionDB.Description, transactionDB.Tag,
		transactionDB.Amount, transactionDB.Date, transactionDB.ID)
	if err != nil {
		return err
	}

	return checkChangesInDb(res)
}

func (tr *transactionRepo) DeleteTransaction(userId, id int64) error {
	res, err := tr.db.Exec(
		`DELETE FROM transactions WHERE id = ? AND
	 	account_id IN (SELECT id FROM accounts WHERE user_id = ?)`,
		id, userId,
	)

	if err != nil {
		return err
	}

	return checkChangesInDb(res)
}
