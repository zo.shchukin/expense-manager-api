package mysql

import (
	"database/sql"

	"gitlab.com/project-una/bb-api/internal/account"
)

type StatementDB struct {
	TransactionDB
	Balance sql.NullFloat64
}

func (s StatementDB) Statement() *account.Statement {
	return &account.Statement{
		Transaction: account.Transaction{
			ID:          s.ID.Int64,
			AccountId:   s.AccountId.Int64,
			Description: s.Description.String,
			Tag:         s.Tag.String,
			Amount:      s.Amount.Float64,
			Date:        s.Date.Time,
		},
		Balance: s.Balance.Float64,
	}
}

type TransactionDB struct {
	ID          sql.NullInt64
	AccountId   sql.NullInt64
	Description sql.NullString
	Tag         sql.NullString
	Amount      sql.NullFloat64
	Date        sql.NullTime
}

func (t TransactionDB) Transaction() *account.Transaction {
	return &account.Transaction{
		ID:          t.ID.Int64,
		AccountId:   t.AccountId.Int64,
		Description: t.Description.String,
		Tag:         t.Tag.String,
		Amount:      t.Amount.Float64,
		Date:        t.Date.Time,
	}
}

func GetTransactionDB(t *account.Transaction) *TransactionDB {
	return &TransactionDB{
		ID:          sql.NullInt64{Int64: t.ID, Valid: true},
		AccountId:   sql.NullInt64{Int64: t.AccountId, Valid: true},
		Description: sql.NullString{String: t.Description, Valid: true},
		Tag:         sql.NullString{String: t.Tag, Valid: true},
		Amount:      sql.NullFloat64{Float64: t.Amount, Valid: true},
		Date:        sql.NullTime{Time: t.Date, Valid: true},
	}
}
