package mysql

import (
	"database/sql"
	"fmt"

	"gitlab.com/project-una/bb-api/internal/account"
)

type validatorRepository struct {
	db *sql.DB
}

func NewValidatorRepository(db *sql.DB) account.ValidatorRepository {
	return &validatorRepository{db}
}

func (vr *validatorRepository) GetAccountOwner(accountId int64) (int64, error) {
	var ownerId int64

	row := vr.db.QueryRow("SELECT user_id FROM accounts WHERE id = ?", accountId)

	err := row.Scan(&ownerId)
	if err != nil {
		return 0, fmt.Errorf("you don't own this account")
	}

	return ownerId, nil

}
