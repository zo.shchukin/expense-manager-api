package mysql

import (
	"database/sql"

	"gitlab.com/project-una/bb-api/internal/account"
)

type AccountDB struct {
	ID       sql.NullInt64
	UserID   sql.NullInt64
	Name     sql.NullString
	Currency sql.NullString
}

func (a AccountDB) Account() *account.Account {
	return &account.Account{
		ID:       a.ID.Int64,
		UserID:   a.UserID.Int64,
		Name:     a.Name.String,
		Currency: a.Currency.String,
	}
}

func GetAccountDB(a *account.Account) *AccountDB {
	return &AccountDB{
		ID:       sql.NullInt64{Int64: a.ID, Valid: true},
		UserID:   sql.NullInt64{Int64: a.UserID, Valid: true},
		Name:     sql.NullString{String: a.Name, Valid: true},
		Currency: sql.NullString{String: a.Currency, Valid: true},
	}
}
