package mysql

import (
	"database/sql"
	"fmt"
	"time"

	"gitlab.com/project-una/bb-api/internal/account"
)

type accountRepo struct {
	db *sql.DB
}

func NewAccountRepository(db *sql.DB) account.AccountRepository {
	return &accountRepo{db}
}

func (ar *accountRepo) GetAllAccounts(userID int64) ([]*account.Account, error) {

	var accounts []*account.Account
	rows, err := ar.db.Query(
		`SELECT id, user_id, name, currency FROM accounts WHERE user_id = ?`, userID,
	)
	if err != nil {
		return nil, err
	}
	defer rows.Close()

	for rows.Next() {
		var date time.Time
		accountDB := &AccountDB{}
		err := rows.Scan(&accountDB.ID, &accountDB.UserID, &accountDB.Name, &accountDB.Currency)
		if err != nil {
			return nil, err
		}

		account := accountDB.Account()
		account.FutureBalance, _ = ar.valueDateBalance(account.ID, date)
		date = time.Now()
		account.CurrentBalance, _ = ar.valueDateBalance(account.ID, date)
		accounts = append(accounts, account)
	}

	return accounts, nil
}

func (ar *accountRepo) GetAccountByID(userID, accId int64) (*account.Account, error) {
	var date time.Time
	row := ar.db.QueryRow(
		`SELECT id, user_id, name, currency FROM accounts WHERE id = ?`, accId,
	)

	accountDB := &AccountDB{}
	err := row.Scan(
		&accountDB.ID, &accountDB.UserID, &accountDB.Name, &accountDB.Currency,
	)
	if err != nil {
		if err == sql.ErrNoRows {
			return nil, fmt.Errorf("account not found")
		}
		return nil, err
	}

	account := accountDB.Account()
	account.FutureBalance, _ = ar.valueDateBalance(account.ID, date)
	date = time.Now()
	account.CurrentBalance, _ = ar.valueDateBalance(account.ID, date)

	return account, nil
}

func (ar *accountRepo) CreateAccount(account *account.Account) (*account.Account, error) {
	accountDB := GetAccountDB(account)
	result, err := ar.db.Exec("INSERT INTO accounts (user_id, name, currency) VALUES (?, ?, ?)",
		accountDB.UserID, accountDB.Name, accountDB.Currency)
	if err != nil {
		return nil, err
	}

	account.ID, err = result.LastInsertId()
	if err != nil {
		return nil, err
	}

	return account, nil
}

func (ar *accountRepo) UpdateAccount(account *account.Account) error {
	accountDB := GetAccountDB(account)

	res, err := ar.db.Exec("UPDATE accounts SET name = ?, currency = ? WHERE id = ? AND user_id = ?",
		accountDB.Name, accountDB.Currency, accountDB.ID, accountDB.UserID)
	if err != nil {
		return err
	}

	return checkChangesInDb(res)
}

func (ar *accountRepo) DeleteAccount(accId int64) error {
	res, err := ar.db.Exec("DELETE FROM accounts WHERE id = ?", accId)
	if err != nil {
		return err
	}

	return checkChangesInDb(res)
}

func (ar *accountRepo) valueDateBalance(accId int64, date time.Time) (float64, error) {
	var (
		balance float64
		pref    string
	)

	if date.Year() > 1 {
		pref = fmt.Sprintf(` AND date <= "%s"`, date.Format("2006-01-02"))
	}
	qwery := "SELECT SUM(amount) FROM transactions WHERE account_id = ?" + pref
	return balance, ar.db.QueryRow(qwery, accId).Scan(&balance)
}

func checkChangesInDb(res sql.Result) error {
	ans, err := res.RowsAffected()
	if err != nil {
		return err
	}

	if ans == 0 {
		return fmt.Errorf("data haven't got changes")
	}

	return nil
}
