package mysql

import (
	"database/sql"

	"gitlab.com/project-una/bb-api/internal/user"
)

type UserDB struct {
	ID           sql.NullInt64
	Name         sql.NullString
	Email        sql.NullString
	HashPassword sql.NullString
}

func (u UserDB) User() *user.User {
	return &user.User{
		ID:       u.ID.Int64,
		Name:     u.Name.String,
		Email:    u.Email.String,
		Password: u.HashPassword.String,
	}
}

func GetUserDB(u *user.User) *UserDB {
	return &UserDB{
		ID:           sql.NullInt64{Int64: u.ID, Valid: true},
		Name:         sql.NullString{String: u.Name, Valid: true},
		Email:        sql.NullString{String: u.Email, Valid: true},
		HashPassword: sql.NullString{String: u.Password, Valid: true},
	}
}
