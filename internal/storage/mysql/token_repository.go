package mysql

import (
	"database/sql"
	"fmt"
	"time"

	"gitlab.com/project-una/bb-api/internal/token"
)

type tokenRepo struct {
	db *sql.DB
}

func NewTokenRepo(db *sql.DB) token.TokenRepository {
	return &tokenRepo{db}
}

func (r *tokenRepo) FindToken(token string) (int, error) {
	var id int
	query := "SELECT id FROM tokens WHERE hash = ?"
	err := r.db.QueryRow(query, token).Scan(&id)
	if err != nil {
		return 0, fmt.Errorf("error : %v", err)
	}

	return id, nil
}

func (r *tokenRepo) AddToken(token string) error {
	date := time.Now()
	_, err := r.db.Exec("INSERT INTO tokens (hash, date) VALUES (?, ?)", token, date)
	if err != nil {
		return err
	}

	return nil
}

func (tr *tokenRepo) DeleteToken(token string) error {
	res, err := tr.db.Exec("DELETE FROM tokens WHERE hash = ?", token)
	if err != nil {
		return err
	}

	return checkChangesInDb(res)
}
