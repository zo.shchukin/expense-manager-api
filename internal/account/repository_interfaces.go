package account

//go:generate mockgen -source=repository_interfaces.go -destination=mocks/mock_repository.go

type TransactionRepository interface {
	GetStatementOfAccount(userId, accId int64) ([]*Statement, error)
	GetCostAnalysis(userId, accId int64) (map[string]float64, error)
	GetTransactionsByAccountID(userId, id int64) ([]*Transaction, error)
	CreateTransaction(transaction *Transaction) (*Transaction, error)
	UpdateTransaction(userId int64, transaction *Transaction) error
	DeleteTransaction(userId, id int64) error
}

type AccountRepository interface {
	GetAllAccounts(userId int64) ([]*Account, error)
	GetAccountByID(userId, accId int64) (*Account, error)
	CreateAccount(account *Account) (*Account, error)
	UpdateAccount(account *Account) error
	DeleteAccount(accId int64) error
}

type ValidatorRepository interface {
	GetAccountOwner(accId int64) (int64, error)
}
