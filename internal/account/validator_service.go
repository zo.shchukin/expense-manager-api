package account

import (
	"fmt"
	"log"
)

type validatorSvc struct {
	repo ValidatorRepository
}

func NewValidatorService(repo ValidatorRepository) ValidatorService {

	return &validatorSvc{repo}
}

func (vs *validatorSvc) IsAccountOwner(userId, accountId int64) (bool, error) {

	ownerId, err := vs.repo.GetAccountOwner(accountId)
	if err != nil {
		return false, err
	}

	if ownerId != userId {
		log.Printf("his id: %v is trying to get data of another user,  accountId: %v", userId, accountId)
		return false, fmt.Errorf("you don't own this account")
	}

	return true, nil
}
