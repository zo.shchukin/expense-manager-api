package account

type accountSvc struct {
	repo      AccountRepository
	validator ValidatorService
}

func NewAccountService(repo AccountRepository, valid ValidatorService) AccountService {
	return &accountSvc{
		repo:      repo,
		validator: valid,
	}
}

func (as *accountSvc) GetAccountByID(userId, acId int64) (*Account, error) {
	if ok, err := as.validator.IsAccountOwner(userId, acId); !ok {
		return nil, err
	}

	return as.repo.GetAccountByID(userId, acId)
}

func (as *accountSvc) GetAllAccounts(userId int64) ([]*Account, error) {

	return as.repo.GetAllAccounts(userId)
}

func (as *accountSvc) CreateAccount(account *Account) (*Account, error) {

	return as.repo.CreateAccount(account)
}

func (as *accountSvc) UpdateAccount(userId int64, account *Account) error {
	if ok, err := as.validator.IsAccountOwner(userId, account.ID); !ok {
		return err
	}

	return as.repo.UpdateAccount(account)
}

func (as *accountSvc) DeleteAccount(userId, acId int64) error {
	if ok, err := as.validator.IsAccountOwner(userId, acId); !ok {
		return err
	}

	return as.repo.DeleteAccount(acId)
}
