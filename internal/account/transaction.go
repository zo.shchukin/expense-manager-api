package account

import (
	"time"
)

type Statement struct {
	Transaction
	Balance float64 `json:"balance"`
}

type Transaction struct {
	ID          int64     `json:"id"`
	AccountId   int64     `json:"account_id"`
	Description string    `json:"description"`
	Tag         string    `json:"tag"`
	Amount      float64   `json:"amount"`
	Date        time.Time `json:"date"`
}
