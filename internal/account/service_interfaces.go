package account

//go:generate mockgen -source=service_interfaces.go -destination=mocks/mock_service.go

type TransactionService interface {
	GetStatementOfAccount(userID, accId int64) ([]*Statement, error)
	GetCostAnalysis(userID, accId int64) (map[string]float64, error)
	GetTransactionsByAccountID(userID, id int64) ([]*Transaction, error)
	CreateTransaction(userID int64, transaction *Transaction) (*Transaction, error)
	UpdateTransaction(userID int64, transaction *Transaction) error
	DeleteTransaction(userID, id int64) error
}

type AccountService interface {
	GetAllAccounts(userID int64) ([]*Account, error)
	GetAccountByID(userID, accId int64) (*Account, error)
	CreateAccount(account *Account) (*Account, error)
	UpdateAccount(userID int64, account *Account) error
	DeleteAccount(userID, accId int64) error
}

type ValidatorService interface {
	IsAccountOwner(userId, accId int64) (bool, error)
}
