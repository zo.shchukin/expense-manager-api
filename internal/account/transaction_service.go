package account

import "log"

type transactionSvc struct {
	repo      TransactionRepository
	validator ValidatorService
}

func NewTransactionService(repo TransactionRepository, valid ValidatorService) TransactionService {

	return &transactionSvc{
		repo:      repo,
		validator: valid,
	}
}

func (ts *transactionSvc) GetStatementOfAccount(userId, accId int64) ([]*Statement, error) {
	if ok, err := ts.validator.IsAccountOwner(userId, accId); !ok {
		log.Printf("user: %v is trying to get data of another user,  accountId: %v", userId, accId)
		return nil, err
	}

	return ts.repo.GetStatementOfAccount(userId, accId)
}

func (ts *transactionSvc) GetCostAnalysis(userId, accId int64) (map[string]float64, error) {
	if ok, err := ts.validator.IsAccountOwner(userId, accId); !ok {
		return nil, err
	}

	return ts.repo.GetCostAnalysis(userId, accId)
}

func (ts *transactionSvc) GetTransactionsByAccountID(userId, accId int64) ([]*Transaction, error) {
	if ok, err := ts.validator.IsAccountOwner(userId, accId); !ok {
		return nil, err
	}

	return ts.repo.GetTransactionsByAccountID(userId, accId)
}

func (ts *transactionSvc) CreateTransaction(userId int64, transaction *Transaction) (*Transaction, error) {
	if ok, err := ts.validator.IsAccountOwner(userId, transaction.AccountId); !ok {
		return nil, err
	}

	return ts.repo.CreateTransaction(transaction)
}

func (ts *transactionSvc) UpdateTransaction(userId int64, transaction *Transaction) error {
	if ok, err := ts.validator.IsAccountOwner(userId, transaction.AccountId); !ok {
		return err
	}

	return ts.repo.UpdateTransaction(userId, transaction)
}

func (ts *transactionSvc) DeleteTransaction(userId, id int64) error {

	return ts.repo.DeleteTransaction(userId, id)
}
