package account

type Account struct {
	ID             int64   `json:"id"`
	UserID         int64   `json:"user_id"`
	Name           string  `json:"name"`
	Currency       string  `json:"currency"`
	FutureBalance  float64 `json:"futureBalance"`
	CurrentBalance float64 `json:"currentBalance"`
}
