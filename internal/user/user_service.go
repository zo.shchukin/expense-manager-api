package user

import (
	"crypto/sha1"
	"fmt"
	"time"
)

const solt = "qfqejifjassfer21312wq"

type service struct {
	repo  UserRepository
	token TokenService
}

func NewService(repo UserRepository, token TokenService) UserService {

	return &service{
		repo:  repo,
		token: token,
	}
}

func (us *service) AuthorizationUser(authForm *AuthForm) (*UserResponse, error) {

	user, err := us.repo.GetUserByEmail(authForm.Email)
	if err != nil {
		return nil, err
	}

	if us.generateHash(authForm.Password) != user.Password {
		return nil, fmt.Errorf("you need authorization")
	}

	tokens, err := us.getTokens(user)
	if err != nil {
		return nil, err
	}

	resp := user.UserResponse()
	resp.Tokens = tokens
	return resp, nil
}

func (us *service) CreateUser(user *User) error {

	user.Password = us.generateHash(user.Password)

	err := us.repo.CreateUser(user)
	if err != nil {
		return err
	}

	return nil
}

func (us *service) RefreshTokens(refreshToken string) (*Tokens, error) {

	err := us.token.VerifyRefresh(refreshToken)
	if err != nil {
		return nil, err
	}

	email, err := us.token.Parse(refreshToken)
	if err != nil {
		return nil, err
	}

	user, err := us.repo.GetUserByEmail(email)
	if err != nil {
		return nil, err
	}

	tokens, err := us.getTokens(user)
	if err != nil {
		return nil, err
	}

	return tokens, err
}

func (us *service) getTokens(user *User) (*Tokens, error) {
	var accTokenTime time.Duration = 15
	var refTokenTime time.Duration = 1440
	token, err := us.token.Generate(fmt.Sprintf("%v", user.ID), accTokenTime)
	if err != nil {
		return nil, err
	}
	refToken, err := us.token.Generate(user.Email, refTokenTime)
	if err != nil {
		return nil, err
	}

	expiresInMilliseconds := int64(accTokenTime) * 60000
	expirationTimestamp := time.Now().UnixNano()/int64(time.Millisecond) + expiresInMilliseconds

	tokens := Tokens{
		AccessToken:  token,
		RefreshToken: refToken,
		ExpiresIn:    expirationTimestamp,
	}

	return &tokens, nil
}

func (us *service) generateHash(str string) string {
	hash := sha1.New()
	hash.Write([]byte(str))
	return fmt.Sprintf("%x", hash.Sum([]byte(solt)))
}
