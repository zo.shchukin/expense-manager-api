package user

import (
	"errors"
	"strconv"
	"testing"
	"time"

	"github.com/golang/mock/gomock"
	"github.com/stretchr/testify/assert"
)

var (
	accTokenTime time.Duration = 15
	refTokenTime time.Duration = 1440

	input = &AuthForm{
		Email:    "test666@test.com",
		Password: "123",
	}

	expUser = &User{
		ID:       18,
		Name:     "third",
		Email:    "test666@test.com",
		Password: "716671656a69666a6173736665723231333132777140bd001563085fc35165329ea1ff5c5ecbdbbeef",
	}

	expTokens = &Tokens{
		AccessToken:  "1234",
		RefreshToken: "12345",
		ExpiresIn:    1689002066704,
	}
)

func TestCreateUserSuccess(t *testing.T) {
	ctl := gomock.NewController(t)
	defer ctl.Finish()

	repo := NewMockUserRepository(ctl)
	tkn := NewMockTokenService(ctl)
	inputUser := &User{
		Name:     "third",
		Email:    "test3@gmail.com",
		Password: "qwerty123",
	}

	repo.EXPECT().CreateUser(inputUser).Return(nil)

	userService := NewService(repo, tkn)
	err := userService.CreateUser(inputUser)

	assert.NoError(t, err)
}

func TestCreateUserError(t *testing.T) {
	ctl := gomock.NewController(t)
	defer ctl.Finish()

	repo := NewMockUserRepository(ctl)
	tkn := NewMockTokenService(ctl)
	inputUser := &User{
		Name:     "third",
		Email:    "test3@gmail.com",
		Password: "qwerty123",
	}
	errRepo := errors.New("repository error")
	repo.EXPECT().CreateUser(inputUser).Return(errRepo)

	userService := NewService(repo, tkn)
	err := userService.CreateUser(inputUser)

	assert.EqualError(t, err, "repository error")
}

func TestAuthorizationUserSuccess(t *testing.T) {
	ctl := gomock.NewController(t)
	defer ctl.Finish()

	repo := NewMockUserRepository(ctl)
	tkn := NewMockTokenService(ctl)

	expResponse := expUser.UserResponse()
	expResponse.Tokens = expTokens

	repo.EXPECT().GetUserByEmail(input.Email).Return(expUser, nil).Times(1)
	tkn.EXPECT().Generate(strconv.Itoa(int(expUser.ID)), accTokenTime).Return("1234", nil)
	tkn.EXPECT().Generate(expUser.Email, refTokenTime).Return("12345", nil)

	userService := NewService(repo, tkn)
	response, err := userService.AuthorizationUser(input)
	response.ExpiresIn = 1689002066704
	assert.Equal(t, expResponse, response)
	assert.NoError(t, err)
}

func TestAuthorizationUserError(t *testing.T) {
	ctl := gomock.NewController(t)
	defer ctl.Finish()

	repo := NewMockUserRepository(ctl)
	tkn := NewMockTokenService(ctl)

	errRepo := errors.New("repository error")

	userService := NewService(repo, tkn)
	repo.EXPECT().GetUserByEmail(input.Email).Return(nil, errRepo).Times(1)

	_, err := userService.AuthorizationUser(input)
	assert.EqualError(t, err, "repository error")
}

func TestAuthorizationUserError1(t *testing.T) {
	ctl := gomock.NewController(t)
	defer ctl.Finish()

	repo := NewMockUserRepository(ctl)
	tkn := NewMockTokenService(ctl)

	errToken := errors.New("token error")
	tkn.EXPECT().Generate(strconv.Itoa(int(expUser.ID)), accTokenTime).Return("1234", nil)
	tkn.EXPECT().Generate(expUser.Email, refTokenTime).Return("", errToken)
	repo.EXPECT().GetUserByEmail(input.Email).Return(expUser, nil).Times(1)

	userService := NewService(repo, tkn)

	_, err := userService.AuthorizationUser(input)
	assert.EqualError(t, err, "token error")
}
