package user

import "time"

//go:generate mockgen -source=interface.go -destination=mock.go

type UserService interface {
	CreateUser(user *User) error
	AuthorizationUser(authForm *AuthForm) (*UserResponse, error)
	RefreshTokens(refreshToken string) (*Tokens, error)
}

type UserRepository interface {
	GetUserByEmail(email string) (*User, error)
	CreateUser(user *User) error
}

type TokenService interface {
	Generate(identity string, ttl time.Duration) (string, error)
	Verify(token string) error
	VerifyRefresh(token string) error
	Parse(token string) (string, error)
	Revoke(token string) error
}
