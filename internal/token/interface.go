package token

//go:generate mockgen -source=interface.go -destination=mock.go
type TokenRepository interface {
	FindToken(token string) (int, error)
	AddToken(token string) error
	DeleteToken(token string) error
}
