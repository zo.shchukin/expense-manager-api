package token

import (
	"crypto/rand"
	"crypto/rsa"
	"testing"
	"time"

	"github.com/golang/mock/gomock"
	"github.com/stretchr/testify/assert"
)

func TestGenerateSuccess(t *testing.T) {
	ctl := gomock.NewController(t)
	defer ctl.Finish()

	repo := NewMockTokenRepository(ctl)
	var inputMin time.Duration = 15
	var inputIdentity = "15"
	privateKey, err := rsa.GenerateKey(rand.Reader, 2048)
	if err != nil {
		panic(err)
	}

	expected, err := `[A-Za-z0-9-_]+\.[A-Za-z0-9-_]+$`, nil
	TokenService := NewTokenService(repo, privateKey)
	res, err := TokenService.Generate(inputIdentity, inputMin)

	assert.Nil(t, err)
	assert.Regexp(t, expected, res)
}

func TestGenerateError(t *testing.T) {
	ctl := gomock.NewController(t)
	defer ctl.Finish()

	repo := NewMockTokenRepository(ctl)
	var inputMin time.Duration = 15
	var inputIdentity = "15"
	privateKey, err := rsa.GenerateKey(rand.Reader, 2048)
	if err != nil {
		panic(err)
	}

	expected, err := `[A-Za-z0-9-_]+\.[A-Za-z0-9-_]+$`, nil
	TokenService := NewTokenService(repo, privateKey)
	res, err := TokenService.Generate(inputIdentity, inputMin)

	assert.Nil(t, err)
	assert.Regexp(t, expected, res)
}
