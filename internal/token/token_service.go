package token

import (
	"crypto/rsa"
	"crypto/sha1"
	"fmt"
	"log"
	"regexp"
	"time"

	"github.com/golang-jwt/jwt/v4"
)

const solt = "qfqejifjassfer21312wq"

var errAuth = fmt.Errorf("you need authorization")

type tokenSvc struct {
	repo       TokenRepository
	privateKey *rsa.PrivateKey
}

func NewTokenService(repo TokenRepository, privateKey *rsa.PrivateKey) *tokenSvc {

	return &tokenSvc{
		repo:       repo,
		privateKey: privateKey,
	}
}

func (ts *tokenSvc) Generate(identity string, ttl time.Duration) (string, error) {
	var token *jwt.Token
	if ok, _ := regexp.MatchString("^[0-9]+$", identity); !ok {

		return ts.generateRefresh(identity, ttl)
	}

	token = jwt.NewWithClaims(jwt.SigningMethodRS256, jwt.MapClaims{
		"jti": identity,
		"exp": time.Now().Add(ttl * time.Minute).Unix(),
		"iss": "budget-buddy",
	})

	tokenString, err := token.SignedString(ts.privateKey)
	if err != nil {
		return "", err
	}

	return tokenString, nil
}

func (ts *tokenSvc) Verify(token string) error {

	t, err := jwt.ParseWithClaims(token, &jwt.RegisteredClaims{},
		func(token *jwt.Token) (interface{}, error) {
			return &ts.privateKey.PublicKey, nil
		})

	if err != nil {
		log.Printf("parseWithClaims: %v", err)
		return errAuth
	}

	if _, ok := t.Claims.(*jwt.RegisteredClaims); ok && t.Valid {

		return nil
	}

	return errAuth
}

func (ts *tokenSvc) VerifyRefresh(token string) error {

	t, err := jwt.ParseWithClaims(token, &jwt.RegisteredClaims{},
		func(token *jwt.Token) (interface{}, error) {
			return &ts.privateKey.PublicKey, nil
		})

	if err != nil {
		log.Printf("parseWithClaims: %v", err)
		return errAuth
	}
	id, err := ts.repo.FindToken(ts.generateHash(token))
	if err != nil || id == 0 {
		return errAuth
	}

	if _, ok := t.Claims.(*jwt.RegisteredClaims); ok && t.Valid {
		log.Println(ts.generateHash(token))
		return ts.repo.DeleteToken(ts.generateHash(token))
	}

	return errAuth
}

func (ts *tokenSvc) Parse(token string) (string, error) {
	t, err := jwt.ParseWithClaims(token, &jwt.RegisteredClaims{},
		func(token *jwt.Token) (interface{}, error) {
			return &ts.privateKey.PublicKey, nil
		},
	)
	if err != nil {
		log.Printf("parseWithClaims: %v", err)
		return "", errAuth
	}

	claims, ok := t.Claims.(*jwt.RegisteredClaims)
	if !ok && !t.Valid {
		return "", errAuth
	}
	if claims.ID == "" {
		log.Println(claims.Subject)
		return claims.Subject, nil
	}
	log.Println(claims.ID)
	return claims.ID, nil
}

func (ts *tokenSvc) Revoke(token string) error {

	return ts.repo.DeleteToken(ts.generateHash(token))
}

func (ts *tokenSvc) generateRefresh(identity string, ttl time.Duration) (string, error) {

	token := jwt.NewWithClaims(jwt.SigningMethodRS256, jwt.MapClaims{
		"sub": identity,
		"exp": time.Now().Add(ttl * time.Minute).Unix(),
		"iss": "budget-buddy",
	})
	tokenString, err := token.SignedString(ts.privateKey)
	if err != nil {
		return "", err
	}

	err = ts.repo.AddToken(ts.generateHash(tokenString))
	if err != nil {
		return "", err
	}

	return tokenString, nil
}

func (ts *tokenSvc) generateHash(str string) string {
	hash := sha1.New()
	hash.Write([]byte(str))
	return fmt.Sprintf("%x", hash.Sum([]byte(solt)))
}
