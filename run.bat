@echo off

echo Stopping and removing existing container...
docker stop bb-api
@REM timeout /t 1

echo Removing existing Docker image...
docker rmi bb_api_image
@REM timeout /t 1

echo Deleting build/app executable...
del build\app
@REM timeout /t 1

echo Building...
docker run --rm -e GOOS=linux -e GOARCH=amd64 -v C:\Users\zoshc\workspace\Golang\FinanceProject:/usr/src/myapp -w /usr/src/myapp golang:1.21.5 go build -o build/app cmd/main.go
@REM timeout /t 1

echo Building Docker image...
docker build -t bb_api_image .
@REM timeout /t 1

echo Running Docker container...
docker run -d --rm --name bb-api -e CONFIG_PATH=config/config.test.yml -e DB_PSW=root -p 81:80 bb_api_image
