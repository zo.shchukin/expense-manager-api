FROM alpine:3.18.2

RUN apk update
RUN apk add ca-certificates
RUN update-ca-certificates

RUN apk add libc6-compat

ADD build/app /usr/src/app/app
ADD config /usr/src/app/config
ADD db/migrations /usr/src/app/db/migrations

RUN ln -s /usr/src/app/app /usr/bin/app

WORKDIR /usr/src/app

ENTRYPOINT ["app"]
