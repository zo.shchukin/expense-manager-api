# Expense manager API

Docker commands for the app:

1. docker run --rm -e GOOS=linux -e GOARCH=amd64 -v C:\Users\zoshc\workspace\Golang\FinanceProject:/usr/src/myapp -w /usr/src/myapp golang:1.21.5 go build -o build/app cmd/main.go
   """create bin app"""
2. docker build -t bb_api_image .
   """create docker image"""
3. docker run -d --rm --name bb-api -e CONFIG_PATH=config/config.test.yml -e DB_PSW=root -p 81:80 bb_api_image
   """create docker continer"""

Docker commands for the database:

1. docker run --name em_api -e MYSQL_ROOT_PASSWORD=root -p 3006:3306 -d mysql:latest
2. docker run --name phpmyadmin_for_em_api --link em_api:mysql -p 8090:80 -e PMA_HOST=mysql -d phpmyadmin
