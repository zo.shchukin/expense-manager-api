package main

import (
	"database/sql"
	"errors"
	"fmt"
	"log"
	"net/http"
	"os"

	"github.com/golang-migrate/migrate/v4"
	"github.com/golang-migrate/migrate/v4/database/mysql"
	_ "github.com/golang-migrate/migrate/v4/source/file"
	"github.com/gorilla/handlers"
	"gitlab.com/project-una/bb-api/internal/app"

	_ "github.com/go-sql-driver/mysql"
)

func main() {
	config := app.GetConfig()

	psw := os.Getenv("DB_PSW")
	dbConnection := fmt.Sprintf(
		config.DB.ConnectionStr, config.DB.Username,
		psw, config.DB.Server, config.Http.Port,
	)

	db, err := sql.Open("mysql", dbConnection)
	if err != nil {
		log.Fatal(err)
	}
	defer db.Close()

	err = db.Ping()
	if err != nil {
		log.Fatal(err)
	}

	err = runMigrations(config, db)
	if err != nil {
		log.Fatal(err)
	}

	router, err := app.Run(db)
	if err != nil {
		log.Fatal(err)
	}

	log.Println("Server started on port 80")
	log.Fatal(http.ListenAndServe(":80", handlers.LoggingHandler(os.Stdout, router)))
}

func runMigrations(config *app.Config, db *sql.DB) error {
	driver, err := mysql.WithInstance(db, &mysql.Config{})
	if err != nil {
		log.Fatal(err)
	}

	m, err := migrate.NewWithDatabaseInstance(
		config.MigratePath,
		"mysql",
		driver,
	)
	if err != nil {
		return fmt.Errorf("migrate.New %v", err)
	}

	if err := m.Up(); err != nil {
		if !errors.Is(err, migrate.ErrNoChange) {
			return fmt.Errorf("m.Up %v", err)
		}
	}

	return nil
}
